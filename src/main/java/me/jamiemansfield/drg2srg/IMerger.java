package me.jamiemansfield.drg2srg;

public interface IMerger<T> {

    T merge(final T first, final T second);

}
