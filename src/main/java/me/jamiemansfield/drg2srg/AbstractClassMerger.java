package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.model.ClassMapping;
import me.jamiemansfield.lorenz.model.FieldMapping;
import me.jamiemansfield.lorenz.model.InnerClassMapping;
import me.jamiemansfield.lorenz.model.MethodMapping;

public abstract class AbstractClassMerger<M extends ClassMapping> implements IMerger<ClassMapping<M>> {

    @Override
    public ClassMapping<M> merge(final ClassMapping<M> first, final ClassMapping<M> second) {
        final ClassMapping<M> newMapping = this.createMapping(first.getDeobfuscatedName());
        final FieldMerger fieldMerger = new FieldMerger(newMapping);
        for (final FieldMapping firstField : first.getFieldMappings()) {
            final FieldMapping secondField = second.getOrCreateFieldMapping(firstField.getObfuscatedName());
            fieldMerger.merge(firstField, secondField);
        }
        final MethodMerger methodMerger = new MethodMerger(newMapping);
        for (final MethodMapping firstMethod : first.getMethodMappings()) {
            final MethodMapping secondMethod = second.getOrCreateMethodMapping(firstMethod.getSignature());
            methodMerger.merge(firstMethod, secondMethod);
        }
        final InnerClassMerger innerClassMerger = new InnerClassMerger(newMapping);
        for (final InnerClassMapping firstInner : first.getInnerClassMappings()) {
            final InnerClassMapping secondInner = second.getOrCreateInnerClassMapping(firstInner.getObfuscatedName());
            innerClassMerger.merge(firstInner, secondInner);
        }
        return newMapping;
    }

    protected abstract ClassMapping<M> createMapping(final String obfuscatedName);

}
