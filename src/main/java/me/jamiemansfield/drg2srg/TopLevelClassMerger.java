package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.MappingSet;
import me.jamiemansfield.lorenz.model.ClassMapping;
import me.jamiemansfield.lorenz.model.TopLevelClassMapping;

public class TopLevelClassMerger extends AbstractClassMerger<TopLevelClassMapping> {

    private final MappingSet mappings;

    public TopLevelClassMerger(final MappingSet mappings) {
        this.mappings = mappings;
    }

    @Override
    protected ClassMapping<TopLevelClassMapping> createMapping(final String obfuscatedName) {
        return this.mappings.getOrCreateTopLevelClassMapping(obfuscatedName);
    }

}
