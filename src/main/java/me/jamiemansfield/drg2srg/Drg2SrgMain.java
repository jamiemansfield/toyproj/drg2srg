package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.MappingSet;
import me.jamiemansfield.lorenz.io.reader.SrgReader;
import me.jamiemansfield.lorenz.io.writer.MappingsWriter;
import me.jamiemansfield.lorenz.io.writer.SrgWriter;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Drg2SrgMain {

    public static void main(final String[] args) throws IOException {
        final MappingSet first = MappingSet.create();
        try (final SrgReader reader = new SrgReader(Files.newBufferedReader(Paths.get("joined.drg")))) {
            reader.parse(first);
        }

        final MappingSet second = MappingSet.create();
        try (final SrgReader reader = new SrgReader(Files.newBufferedReader(Paths.get("joined.srg")))) {
            reader.parse(second);
        }

        final MappingSet drg2Srg = new MappingSetMerger().merge(first, second);
        try (final MappingsWriter writer = new SrgWriter(new PrintWriter(Files.newOutputStream(Paths.get("drg2srg.srg"))))) {
            writer.write(drg2Srg);
        }
    }

    private Drg2SrgMain() {
    }

}
