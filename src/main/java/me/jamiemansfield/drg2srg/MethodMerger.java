package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.model.ClassMapping;
import me.jamiemansfield.lorenz.model.MethodMapping;
import me.jamiemansfield.lorenz.model.jar.MethodDescriptor;
import me.jamiemansfield.lorenz.model.jar.signature.MethodSignature;

public class MethodMerger implements IMerger<MethodMapping> {

    private final ClassMapping parent;

    public MethodMerger(final ClassMapping parent) {
        this.parent = parent;
    }

    @Override
    public MethodMapping merge(final MethodMapping first, final MethodMapping second) {
        final MethodSignature signature = new MethodSignature(
                first.getDeobfuscatedName(),
                MethodDescriptor.compile(first.getDeobfuscatedDescriptor())
        );
        return this.parent.getOrCreateMethodMapping(signature)
                .setDeobfuscatedName(second.getDeobfuscatedName());
    }
}
