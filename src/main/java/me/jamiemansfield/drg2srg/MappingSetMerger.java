package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.MappingSet;
import me.jamiemansfield.lorenz.model.TopLevelClassMapping;

public class MappingSetMerger implements IMerger<MappingSet> {

    @Override
    public MappingSet merge(final MappingSet first, final MappingSet second) {
        final MappingSet mappings = MappingSet.create();
        final TopLevelClassMerger topLevelClassMerger = new TopLevelClassMerger(mappings);
        for (final TopLevelClassMapping firstClass : first.getTopLevelClassMappings()) {
            final TopLevelClassMapping secondClass = second.getOrCreateTopLevelClassMapping(firstClass.getObfuscatedName());
            topLevelClassMerger.merge(firstClass, secondClass);
        }
        return mappings;
    }

}
