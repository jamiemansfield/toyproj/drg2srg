package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.model.ClassMapping;
import me.jamiemansfield.lorenz.model.InnerClassMapping;

public class InnerClassMerger extends AbstractClassMerger<InnerClassMapping> {

    private final ClassMapping parent;

    public InnerClassMerger(final ClassMapping parent) {
        this.parent = parent;
    }

    @Override
    protected ClassMapping<InnerClassMapping> createMapping(final String obfuscatedName) {
        return this.parent.getOrCreateInnerClassMapping(obfuscatedName);
    }

}
