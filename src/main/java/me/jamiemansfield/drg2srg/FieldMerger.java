package me.jamiemansfield.drg2srg;

import me.jamiemansfield.lorenz.model.ClassMapping;
import me.jamiemansfield.lorenz.model.FieldMapping;

public class FieldMerger implements IMerger<FieldMapping> {

    private final ClassMapping parent;

    public FieldMerger(final ClassMapping parent) {
        this.parent = parent;
    }

    @Override
    public FieldMapping merge(final FieldMapping first, final FieldMapping second) {
        return this.parent.getOrCreateFieldMapping(first.getDeobfuscatedName())
                .setDeobfuscatedName(second.getDeobfuscatedName());
    }

}
